#!/bin/sh

g++ main.cpp game.cpp map.cpp systems.cpp factory.cpp fsm.cpp \
    -o game -std=c++17 -Wall \
    -I../vendor/GPGOAP/ -I../vendor/entt/src/ -I../vendor/SFML/include/ -I../vendor/TGUI-0.8.5/include/TGUI/ \
    -L../vendor/SFML/build/lib/ -L../vendor/TGUI-0.8.5/build/lib/ -L../vendor/GPGOAP/lib/ \
    -lsfml-graphics -lsfml-window -lsfml-network -lsfml-system -ltgui
