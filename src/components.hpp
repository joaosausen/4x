#ifndef COMPONENTS_HPP
#define COMPONENTS_HPP

#define TILE_SIZE 16
#include <vector>
#include <unordered_map>
#include <string>
#include <SFML/Graphics.hpp>
#include <entt/entt.hpp>

// The entity position in the grid, this gets updated when entities move.
struct position_component {
  int x;
  int y;
};

// Not a component.
struct S_path {
  position_component pos;
  double cost;
};

struct movement_component {
  float velocity;
  float movement_points;
  float max_movement_points;
  std::vector<S_path> destinations; // Fifo of destinations.
};

// Metadata
struct metadata_component {
  std::string name;
};

// NONE is if there is no ai (the player)
// PLACEHOLDER just passes turns.
enum E_ai { NONE, PLACEHOLDER, PASSIVE };
struct player_component {
  std::string name;
  E_ai ai;
  std::vector<entt::entity> allies;
  std::vector<entt::entity> enemies;
};

// Check if we can click on the unit and display additional info.
struct select_component {
  bool selected;
  int width;
  int height;
  entt::entity player;
};

// Component for settlement and all data used by settlements.
struct settlement_component {
  entt::entity player;
};

struct army_component {
  entt::entity player;
};

// Unit is renderable and static.
struct render_component {
  sf::Sprite sprite;

  const sf::Sprite & get_sprite() {
    return sprite;
  };
};

/** @todo move this to a state manager or FSM. */
enum E_state { IDLE, WALK };
enum E_direction { N = 0, NE = 1, E = 2, SE = 3, S = 4, SW = 5, W = 6, NW = 7 };
/**                ⬆            ⬈             ➡           ⬊             ⬇            ⬋            ⬅            ⬉              */

// This one is not really a component.
struct S_animation_sprite {
  sf::Sprite sprite; // To mirror "sprite.setTextureRect(sf::IntRect(width, 0, -width, height));"
  int duration; // The duration in milisecond for the sprite.

  S_animation_sprite(sf::Sprite _sprite, int _duration) : sprite(_sprite), duration(_duration) { };

  S_animation_sprite(sf::Sprite _sprite, int _duration, sf::Vector2f _origin) :
    sprite(_sprite), duration(_duration) {
    sprite.setOrigin(_origin);
  };
};

// Unit is renderable and animated.

typedef std::unordered_map<E_state, std::unordered_map<E_direction, std::vector<S_animation_sprite>>> T_animations;
struct animate_component {
  E_state state; // The actual state being rendered.
  E_direction direction;
  int state_sprite_delta; // The sprite from "animation_group" vector that is being rendered.
  int accumulated_time;
  T_animations animations;
  sf::Vector2f position;

  S_animation_sprite & get_animation_sprite() {
    return animations[state][direction][state_sprite_delta];
  };

  sf::Sprite & get_sprite() {
    return get_animation_sprite().sprite;
  };
};


/**
 * hash for unordered_map, unordered_set, etc.
 */
namespace std {
  template <> struct hash<position_component> {
    std::size_t operator()(const position_component &id) const noexcept {
      return std::hash<int>()(id.x ^ (id.y << 4));
    }
  };
}

#endif
