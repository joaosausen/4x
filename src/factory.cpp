#include "factory.hpp"
#include <entt/entity/registry.hpp>

void f_settlement(entt::registry &registry, int x, int y, std::string name, const sf::Texture &texture, entt::entity player) {
  auto entity = registry.create();
  auto sprite = sf::Sprite(texture, sf::Rect(6 * TILE_SIZE, 5 * TILE_SIZE, TILE_SIZE, TILE_SIZE));
  sprite.setPosition(x * TILE_SIZE, y * TILE_SIZE);
  registry.assign<position_component>(entity, x, y);
  registry.assign<render_component>(entity, sprite);
  registry.assign<metadata_component>(entity, name);
  registry.assign<select_component>(entity, false, TILE_SIZE, TILE_SIZE, player);
  registry.assign<settlement_component>(entity, player);
}

void f_army(entt::registry &registry, int x, int y, const sf::Texture &texture, entt::entity player) {
  auto entity = registry.create();
  registry.assign<position_component>(entity, x, y); // @todo fix.
  registry.assign<movement_component>(entity, 10.f, 50.f, 50.f, std::vector<S_path>{});
  registry.assign<select_component>(entity, false, TILE_SIZE, TILE_SIZE, player); // @todo fix.
  registry.assign<army_component>(entity, player);

  // Add animation.
  // @todo Properly load animations from external file.
  T_animations animations;
  const auto time = 80; // @todo remove and load from external file.
  animations[E_state::IDLE][E_direction::NW] = {{{texture, {0,  0,  22, 40}}, 0, {3, 24}}};
  animations[E_state::IDLE][E_direction::N]  = {{{texture, {22, 0,  22, 40}}, 0, {3, 24}}};
  animations[E_state::IDLE][E_direction::NE] = {{{texture, {44, 0,  22, 40}}, 0, {3, 24}}};
  animations[E_state::IDLE][E_direction::E]  = {{{texture, {66, 0,  22, 40}}, 0, {3, 24}}};
  animations[E_state::IDLE][E_direction::SE] = {{{texture, {0,  40, 22, 40}}, 0, {3, 24}}};
  animations[E_state::IDLE][E_direction::S]  = {{{texture, {22, 40, 22, 40}}, 0, {3, 24}}};
  animations[E_state::IDLE][E_direction::SW] = {{{texture, {44, 40, 22, 40}}, 0, {3, 24}}};
  animations[E_state::IDLE][E_direction::W]  = {{{texture, {66, 40, 22, 40}}, 0, {3, 24}}};

  animations[E_state::WALK][E_direction::NW] = {
    {{texture, {0,   80,  22, 40}}, time, {3, 24}},
    {{texture, {22,  80,  22, 40}}, time, {3, 24}},
    {{texture, {44,  80,  22, 40}}, time, {3, 24}},
    {{texture, {66,  80,  22, 40}}, time, {3, 24}},
  };
  animations[E_state::WALK][E_direction::N] = {
    {{texture, {0,   120,  22, 40}}, time, {3, 24}},
    {{texture, {22,  120,  22, 40}}, time, {3, 24}},
    {{texture, {44,  120,  22, 40}}, time, {3, 24}},
    {{texture, {66,  120,  22, 40}}, time, {3, 24}},
  };
  animations[E_state::WALK][E_direction::NE] = {
    {{texture, {0,   160,  22, 40}}, time, {3, 24}},
    {{texture, {22,  160,  22, 40}}, time, {3, 24}},
    {{texture, {44,  160,  22, 40}}, time, {3, 24}},
    {{texture, {66,  160,  22, 40}}, time, {3, 24}},
  };
  animations[E_state::WALK][E_direction::E] = {
    {{texture, {0,   200,  22, 40}}, time, {3, 24}},
    {{texture, {22,  200,  22, 40}}, time, {3, 24}},
    {{texture, {44,  200,  22, 40}}, time, {3, 24}},
    {{texture, {66,  200,  22, 40}}, time, {3, 24}},
  };
  animations[E_state::WALK][E_direction::SE] = {
    {{texture, {0,   240,  22, 40}}, time, {3, 24}},
    {{texture, {22,  240,  22, 40}}, time, {3, 24}},
    {{texture, {44,  240,  22, 40}}, time, {3, 24}},
    {{texture, {66,  240,  22, 40}}, time, {3, 24}},
  };
  animations[E_state::WALK][E_direction::S] = {
    {{texture, {0,   280,  22, 40}}, time, {3, 24}},
    {{texture, {22,  280,  22, 40}}, time, {3, 24}},
    {{texture, {44,  280,  22, 40}}, time, {3, 24}},
    {{texture, {66,  280,  22, 40}}, time, {3, 24}},
  };
  animations[E_state::WALK][E_direction::SW] = {
    {{texture, {0,   320,  22, 40}}, time, {3, 24}},
    {{texture, {22,  320,  22, 40}}, time, {3, 24}},
    {{texture, {44,  320,  22, 40}}, time, {3, 24}},
    {{texture, {66,  320,  22, 40}}, time, {3, 24}},
  };
  animations[E_state::WALK][E_direction::W] = {
    {{texture, {0,   360,  22, 40}}, time, {3, 24}},
    {{texture, {22,  360,  22, 40}}, time, {3, 24}},
    {{texture, {44,  360,  22, 40}}, time, {3, 24}},
    {{texture, {66,  360,  22, 40}}, time, {3, 24}},
  };
  registry.assign<animate_component>(entity, E_state::WALK, E_direction::S, 0, 0, animations, sf::Vector2f{(float) (x * TILE_SIZE), (float) (y * TILE_SIZE)});
}
