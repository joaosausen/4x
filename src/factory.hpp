#pragma once
#define TILE_SIZE 16
#include <SFML/Graphics.hpp>
#include <entt/entity/registry.hpp>
#include <unordered_map>
#include "components.hpp"

void f_settlement(entt::registry&, int, int, std::string, const sf::Texture&, entt::entity);
void f_army(entt::registry&, int, int, const sf::Texture&, entt::entity);
