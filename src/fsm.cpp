#include "fsm.hpp"

/**
 * There is no FSM yet, just a helper function.
 */

void transition_animation_state(const E_state state, animate_component &ani) {
  // Prevent transition if state is the same, that way we don't reset the sprite
  // delta.
  if (state != ani.state) {
    if (ani.animations.find(state) != ani.animations.end()) {
      if (ani.animations[state].size() > 0) {
        ani.state_sprite_delta = 0;
        ani.state = state;
      }
    }
  }
}
