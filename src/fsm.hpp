#pragma once
#include "components.hpp"

/**
 * There is no FSM yet, just a helper function.
 */
void transition_animation_state(const E_state, animate_component&);
