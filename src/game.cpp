#include "game.hpp"
#include "scene_lobby.hpp"
#include "scene_world_map.hpp"

game::game::game(sf::RenderWindow &window, sf::View &view) 
  : window(window), view(view) {

  // Load textures.
  texture_cache.load<texture_loader>("map"_hs, "assets/map.png");
  texture_cache.load<texture_loader>("spritesheet/human"_hs, "assets/human_sprite.png");
  texture_cache.load<texture_loader>("spritesheet/alien"_hs, "assets/alien_sprite.png");

  // Init lobby scene.
  auto * scene_lobby = new SceneLobby(window, this);
  add_scene(scene_lobby);
}

Scene * game::game::get_active_scene() {
  return scenes.back();
}

void game::game::add_scene(Scene * scene) {
  scenes.push_back(scene);
}

/**
 * Misc functions.
 */

// Start the game, init the world map scene.
void game::game::start_game() {
  auto * scene_world_map = new SceneWorldMap(window, this);
  add_scene(scene_world_map);
}
