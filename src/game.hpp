#pragma once
#include <vector>
#include <SFML/Graphics.hpp>
#include "scene.hpp"

/**
 * Texture loader struct.
 *
 * Extends entt loader to create a cache for textures.
 */
struct texture_loader final : entt::resource_loader<texture_loader, sf::Texture> {
  std::shared_ptr<sf::Texture> load(std::string path) const {
    auto texture = new sf::Texture();
    texture->loadFromFile(path);
    return std::shared_ptr<sf::Texture>(texture);
  }
};

namespace game {

/**
 * Class game.
 */
class game {
  private:
    std::vector<Scene *> scenes;

  public:
    sf::RenderWindow &window;
    sf::View &view;
    entt::entity player; // A reference to this player.
    std::vector<entt::entity> players; // All players.

    // Constructor
    game(sf::RenderWindow&, sf::View&);

    // Scenes
    void add_scene(Scene *);
    Scene * get_active_scene();

    // Misc.
    void start_game();

    // Cache, resources management.
    entt::resource_cache<sf::Texture> texture_cache{};
};

}
