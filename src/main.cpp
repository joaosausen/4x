#define TILE_SIZE 16
#define TGUI_USE_CPP17 TRUE

#include <entt/entt.hpp>
#include <SFML/Graphics.hpp>
#include <TGUI/TGUI.hpp>
#include "game.hpp"
#include "map.hpp"
#include "scene_lobby.hpp"
#include "scene_world_map.hpp"

int main() {
  sf::RenderWindow window{{800, 600}, "Game"};
  window.setFramerateLimit(60);
  sf::View view(sf::FloatRect(0.f, 0.f, 800.f, 600.f));
  window.setView(view);

  game::game game(window, view);
  
  sf::Clock clock;
  while (window.isOpen()) {
    auto active_scene = game.get_active_scene();

    // Events.
    sf::Event event;
    while (window.pollEvent(event)) {
      if (event.type == sf::Event::Resized) {
        sf::FloatRect visibleArea(0.f, 0.f, event.size.width, event.size.height);
        window.setView(sf::View(visibleArea));
      }
      if (event.type == sf::Event::Closed) {
        window.close();
      }
      else {
        active_scene->event_handler(event);
      }        
    }
       
    // Tick.
    auto dt = clock.getElapsedTime().asMilliseconds();
    active_scene->tick(dt);
    clock.restart();

    // Render.
    active_scene->render();
  }
}
