#include "map.hpp"

// Operations on positions.
bool operator == (position_component a, position_component b) {
  return a.x == b.x && a.y == b.y;
}
bool operator != (position_component a, position_component b) {
  return !(a == b);
}
bool operator < (position_component a, position_component b) {
  return std::tie(a.x, a.y) < std::tie(b.x, b.y);
}

game::map::map() { }
game::map::map(game * ptr_game, int width, int height) : ptr_game(ptr_game), width(width), height(height) { }

void game::map::load(const sf::Texture &texture, std::vector<int> tiles) {
  const int map_width = width;
  const int tileset_width = texture.getSize().x / TILE_SIZE;

  int mapx, mapy, tilex, tiley;
  char cost;
  for (unsigned long i = 0; i < tiles.size(); i++) {
    auto tile = tiles[i];
    
    mapx = i % map_width;
    mapy = i / map_width;
    tilex = (tile - 1) % tileset_width;
    tiley = (tile - 1) / tileset_width;

    auto sprite = sf::Sprite(texture, sf::Rect(tilex * TILE_SIZE, tiley * TILE_SIZE, TILE_SIZE, TILE_SIZE));
    sprite.setPosition(mapx * TILE_SIZE, mapy * TILE_SIZE);
    position_component pos{mapx, mapy};
    sprites.insert({pos, sprite});

    cost = 3;
    // map weigths.
    if (tile >= 62 && tile <= 88 && tile != 68) {
      cost = 1; // Road.
    }
    else {
      switch (tile) {
        case 13: cost = 15; break; // Mountain.
        case 7:  cost = 7;  break; // Forest.
      }
    }

    
    weights.insert({pos, cost});
  }

  refresh();
}

void game::map::refresh() {
  map_texture.create(width * TILE_SIZE, height * TILE_SIZE);
  map_texture.clear(sf::Color::Black);
  for (auto tile : sprites) {
    map_texture.draw(tile.second);
  }
  map_texture.display();
  map_sprite.setTexture(map_texture.getTexture());
}
       
// Check if position is inside the map.
bool game::map::in_bounds(position_component id) const {
  return 0 <= id.x && id.x < width && 0 <= id.y && id.y < height;
}

// Check if position is passable.
bool game::map::passable(position_component id) const {
  return walls.find(id) == walls.end();
}

// Return neighbors.
std::vector<position_component> game::map::neighbors(position_component id) const {
  std::vector<position_component> results;
  for (position_component dir : DIRS) {
    position_component next{id.x + dir.x, id.y + dir.y};
    if (in_bounds(next) && passable(next)) {
      results.push_back(next);
    }
  }
  if ((id.x + id.y) % 2 == 0) {
    std::reverse(results.begin(), results.end());
  }
  return results;
}

// Calculate the cost to go from_node to to_node.
double game::map::cost(position_component from_node, position_component to_node) const {
  double weight = weights.find(to_node) != weights.end() ? weights.at(to_node) : 1;
  // If diagonal, add a factor of diagonal.
  if (from_node.x != to_node.x && from_node.y != to_node.y) {
    weight = sqrt((weight * weight) * 2);
  }
  return weight;
}

inline double game::map::heuristic(position_component a, position_component b) {
  return std::abs(a.x - b.x) + std::abs(a.y - b.y);
}
  
void game::map::a_star_search(position_component start, position_component goal, std::unordered_map<position_component, position_component> &came_from, std::unordered_map<position_component, double> &cost_so_far) {
  PriorityQueue<position_component, double> frontier;
  frontier.put(start, 0);

  came_from[start] = start;
  cost_so_far[start] = 0;
  
  while (!frontier.empty()) {
    position_component current = frontier.get();
    if (current == goal) {
      break;
    }
    for (position_component next : neighbors(current)) {
      double new_cost = cost_so_far[current] + cost(current, next);
      if (cost_so_far.find(next) == cost_so_far.end() || new_cost < cost_so_far[next]) {
        cost_so_far[next] = new_cost;
        double priority = new_cost + heuristic(next, goal);
        frontier.put(next, priority);
        came_from[next] = current;
      }
    }
  }
}

void game::map::draw() {
  ptr_game->window.draw(map_sprite);
}

std::vector<S_path> game::map::calculate_pathfind(position_component start, position_component dest) {
  std::unordered_map<position_component, position_component> came_from;
  std::unordered_map<position_component, double> cost_so_far;
  a_star_search(start, dest, came_from, cost_so_far);

  // Rebuild the path.
  std::vector<S_path> pathfinding_path;
  auto current = dest;
  double cost = 0;
  while (current != start) {
    // The vector goes from dest to start, so we calculate the cost subtracting
    // the next cost from the actual cost.
    cost = cost_so_far[current] - cost_so_far[came_from[current]];
    pathfinding_path.push_back(S_path{current, cost});
    current = came_from[current];
  }
  // Uncommenting this will add the start position to the pathfinding path.
  //pathfinding_path.push_back(S_path {start, 0});
  std::reverse(pathfinding_path.begin(), pathfinding_path.end());

  return pathfinding_path;
}
