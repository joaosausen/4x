#pragma once
#define TILE_SIZE 16
#include <iomanip>
#include <unordered_map>
#include <unordered_set>
#include <array>
#include <vector>
#include <utility>
#include <queue>
#include <tuple>
#include <algorithm>
#include <cstdlib>
#include <math.h>
#include "components.hpp"
#include <entt/entt.hpp>
#include <SFML/Graphics.hpp>
#include "game.hpp"

namespace game {
  
struct map {

  public:
    constexpr static position_component DIRS[8] = {
      position_component{-1, -1}, position_component{0, -1}, position_component{1, -1},
      position_component{-1,  0},      /* MIDDLE */         position_component{1,  0},
      position_component{-1,  1}, position_component{0,  1}, position_component{1,  1},
    };

    game * ptr_game;
    sf::RenderTexture map_texture; // Cache.
    sf::Sprite map_sprite; // Cache.
    std::unordered_map<position_component, sf::Sprite> sprites;
    std::unordered_set<position_component> walls;
    std::unordered_map<position_component, char> weights;
    int width;
    int height;

    map();
    map(game*, int, int);

    bool in_bounds(position_component id) const;
    bool passable(position_component id) const;
    std::vector<position_component> neighbors(position_component id) const;
    double cost(position_component from_node, position_component to_node) const;
    void a_star_search(position_component, position_component, std::unordered_map<position_component, position_component>&, std::unordered_map<position_component, double>&);
    std::vector<S_path> calculate_pathfind(position_component, position_component);
    inline double heuristic(position_component, position_component);
    void load(const sf::Texture&, std::vector<int>);
    void draw();
    void refresh(); // Print the map to a texture.
    
  };

  template<typename T, typename priority_t>
  struct PriorityQueue {
    typedef std::pair<priority_t, T> PQElement;
    std::priority_queue<PQElement, std::vector<PQElement>, std::greater<PQElement>> elements;

    inline bool empty() const {
       return elements.empty();
    }

    inline void put(T item, priority_t priority) {
      elements.emplace(priority, item);
    }

    T get() {
      T best_item = elements.top().second;
      elements.pop();
      return best_item;
    }
  };

}
