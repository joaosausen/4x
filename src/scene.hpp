#pragma once
#include <entt/entt.hpp>
#include <SFML/Graphics.hpp>
#include <TGUI/TGUI.hpp>
#include "game.hpp"

// Forward declaration.
namespace game {
  class game;
}

/**
 * Scene class.
 */
class Scene {

  public:
    virtual void tick(int) { }
    virtual void render() { }
    virtual void event_handler(sf::Event) { }

};
