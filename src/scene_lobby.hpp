#pragma once
#include "scene.hpp"
#include "game.hpp"

/**
 * SceneLobby class.
 */
class SceneLobby : public Scene {

  private:
    tgui::Gui gui;
    entt::registry registry;
    sf::RenderWindow &window;
    game::game * game;

  public:

    void tick(int dt) { }

    void render() {
      window.clear();
      gui.draw();
      window.display();
    }

    void event_handler(sf::Event event) {
      gui.handleEvent(event);
    }

    SceneLobby(sf::RenderWindow &window, game::game * game)
      : window(window), game(game) {
      gui.setTarget(window);
      // Start gui.
      auto gui_start_game = tgui::Button::create();
      gui_start_game->setPosition(75, 70);
      gui_start_game->setText("Start Game");
      gui_start_game->setSize(100, 30);
      gui_start_game->connect("pressed", [=](){ game->start_game(); });
      gui.add(gui_start_game);

      // @todo Need to pass the players to the next scene somehow.
    }

};
