#pragma once
#define TILE_SIZE 16
#include <vector>
#include <cstdio>

#include "scene.hpp"
#include "game.hpp"
#include "map.hpp"
#include "systems.hpp"
#include "factory.hpp"

/**
 * SceneLobby class.
 */
class SceneWorldMap : public Scene {

  private:
    tgui::Gui gui;
    entt::registry registry;
    sf::RenderWindow &window;
    game::game * game;
    game::map map;

    // A list of all players is stored in the game, this is the index of the
    // player that is playing now.
    size_t player_turn; 

    // Misc variables.
    std::vector<entt::entity> selected_entity;
    bool selected_entity_is_movable = false;
    sf::Vector2i mouse_pos, old_mouse_pos; // Mouse position translated to world position.
    sf::Vector2f mouse_world_pos, old_mouse_world_pos; // Mouse position translated to world position.
    bool recalculate_pathfind = false; // This must start as false.
    std::vector<S_path> pathfinding_route;

  public:

    void tick(int dt) {
      animation_system(registry, dt);
      movement_system(registry, dt);
      // Consider first selected entity only. @todo (might need a fix if multiple items are selectable)
      if (recalculate_pathfind && !selected_entity.empty() && registry.valid(selected_entity.front())) {
        int tile_x = mouse_world_pos.x / TILE_SIZE;
        int tile_y = mouse_world_pos.y / TILE_SIZE;
        auto pos = registry.get<position_component>(selected_entity.front());
        pathfinding_route = map.calculate_pathfind(position_component{pos.x, pos.y}, position_component{(int) tile_x, (int) tile_y});
        
        recalculate_pathfind = false;
      }
    }

    void render() {
      window.clear();    
      map.draw();
      if (!selected_entity.empty() && selected_entity_is_movable) {
        render_pathfind_system(registry, pathfinding_route, selected_entity, window);
      }
      render_system(registry, window);
      //render_armies_system(registry, game);
      gui.draw();
      window.display();
    }

    void event_handler(sf::Event event) {
      mouse_pos = sf::Mouse::getPosition(window);
      mouse_world_pos = window.mapPixelToCoords(mouse_pos);
      auto mouse_offset = old_mouse_pos - mouse_pos;
      
      if (event.type == sf::Event::MouseButtonReleased) {
        // Left button.
        if (event.mouseButton.button == 0) {
          selected_entity = world_handle_click(registry, mouse_world_pos, game->player);
          if (!selected_entity.empty()) {
            selected_entity_is_movable = registry.has<movement_component>(selected_entity.front());
          }
          else {
            selected_entity_is_movable = false;
          }
          if (selected_entity.empty() || !registry.valid(selected_entity.front()) || !selected_entity_is_movable) {
            pathfinding_route.clear();
          }
        }
        // Right button.
        else if (event.mouseButton.button == 1) {
          if (!pathfinding_route.empty() && !selected_entity.empty() && registry.valid(selected_entity.front())) {
            auto &mov = registry.get<movement_component>(selected_entity.front());
            // Stop.
            mov.destinations.clear();
            mov.destinations = pathfinding_route;
            // Should start moving now.
            pathfinding_route.clear();
          }
        }
      }
      else if (event.type == sf::Event::MouseMoved) {
        if (sf::Mouse::isButtonPressed(sf::Mouse::Middle)) {
          game->view.move(sf::Vector2f(mouse_offset));
          game->window.setView(game->view);
        }
        else {
          if (!selected_entity.empty() && registry.valid(selected_entity.front()) && selected_entity_is_movable) {
            // Check if mouse moved to a different tile.
            if ((mouse_world_pos.x / TILE_SIZE != old_mouse_world_pos.y / TILE_SIZE)
            || (mouse_world_pos.y / TILE_SIZE != old_mouse_world_pos.y / TILE_SIZE)) {
              recalculate_pathfind = true;
            }
          }
        }
      }
      else if (event.type == sf::Event::MouseWheelScrolled) {
        if (event.mouseWheelScroll.wheel == sf::Mouse::VerticalWheel) {
          game->view.zoom(event.mouseWheelScroll.delta > 0 ? 0.8f : 1.25f);
          game->window.setView(game->view);
        }
      }

      // Resize should reposition GUI.
      if (event.type == sf::Event::Resized) {
        sf::FloatRect visibleArea(0.f, 0.f, event.size.width, event.size.height);
        gui.setView(sf::View(visibleArea));
        refresh_gui();        
      }

      old_mouse_pos = mouse_pos;
      old_mouse_world_pos = mouse_world_pos;
      gui.handleEvent(event);
    }

    /**
     * Construct and init the scene.
     */
    SceneWorldMap(sf::RenderWindow &window, game::game * game)
      : window(window), game(game) {
      gui.setTarget(window);

      // Create players, this must come from the lobby. @todo
      auto player = registry.create();
      auto &player_c = registry.assign<player_component>(player, "Aliens", E_ai::NONE);
      // Create npc and add player as enemy.
      auto npc = registry.create();
      auto &npc_c = registry.assign<player_component>(npc, "Humans", E_ai::PLACEHOLDER);
      // Assign npc as enemy of player.
      player_c.enemies.push_back(npc);
      npc_c.enemies.push_back(player);

      game->players.push_back(player);
      game->players.push_back(npc);
      player_turn = 0;

      // Set player as the main human player.
      game->player = player;

      // Start map.
      auto handle = game->texture_cache.handle("map"_hs);
      const auto &texture = handle;
      map.ptr_game = game;
      map.height = 100;
      map.width = 100;
      map.load(texture, {13,13,13,13,13,7,7,7,13,13,13,13,13,13,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,7,5,13,13,13,13,13,13,13,13,13,13,13,13,13,13,7,7,16,72,72,72,73,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,7,5,13,13,13,13,13,13,13,13,13,13,13,13,13,13,7,7,7,7,7,7,16,1,1,1,1,1,1,1,1,1,1,1,1,1,1,16,72,72,72,72,72,67,72,72,72,72,73,1,1,1,1,16,72,72,72,72,72,72,73,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,7,5,13,13,13,13,13,13,13,13,13,13,7,7,7,7,7,7,7,7,7,7,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,78,1,1,1,1,78,1,1,1,1,78,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,7,5,13,13,13,13,13,13,13,13,13,13,7,7,7,7,7,7,7,13,13,7,85,72,72,72,72,72,72,16,72,72,72,72,72,72,72,87,1,1,1,1,1,78,1,1,1,1,78,1,1,1,1,78,1,1,1,1,1,1,85,72,72,72,72,72,72,72,72,72,72,72,72,72,73,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,7,5,13,13,13,13,13,13,13,13,13,13,7,7,7,7,7,13,13,13,13,13,7,7,7,7,7,7,7,80,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,78,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,7,5,5,5,5,5,5,5,5,5,13,13,7,7,7,7,13,13,13,13,13,13,13,13,13,13,13,7,7,80,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,85,86,86,86,86,87,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,16,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,7,7,7,7,7,7,7,7,7,7,13,7,7,7,7,13,13,13,13,13,13,13,13,13,13,13,13,13,7,80,1,1,1,1,1,1,1,1,1,1,1,1,1,16,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,67,72,72,72,72,72,72,72,73,1,1,1,1,1,1,1,1,1,7,16,7,7,7,7,13,13,13,13,13,13,13,13,13,13,13,13,7,80,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,85,72,72,72,67,72,72,72,72,72,87,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,16,1,1,1,1,1,1,1,1,1,7,78,7,7,7,7,7,13,13,13,13,13,13,13,13,13,13,7,7,80,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,7,78,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,80,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,7,85,86,67,72,72,72,72,72,72,72,72,73,7,7,7,7,7,7,80,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,13,13,13,16,1,1,1,1,1,1,1,1,85,72,72,72,72,72,72,79,86,86,86,86,86,86,86,86,86,86,86,86,86,65,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,85,73,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,13,13,13,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,71,86,86,86,86,86,86,16,73,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,71,72,72,87,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,64,86,86,86,73,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,85,86,16,73,1,1,1,1,1,1,1,1,1,1,80,1,1,1,85,72,72,72,72,16,72,72,72,72,72,72,72,65,7,7,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,85,72,16,72,72,72,72,73,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,7,7,80,7,7,7,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,71,72,72,72,72,72,87,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,71,86,86,86,87,1,1,1,1,1,1,1,1,1,1,1,1,1,7,7,7,85,73,7,7,7,1,1,1,80,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,7,7,7,7,7,80,7,7,7,7,1,1,85,86,86,86,86,86,86,86,86,86,86,16,72,72,72,73,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,7,7,7,7,7,7,80,7,7,7,7,7,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,78,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,16,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,7,7,7,7,7,7,71,87,7,7,7,7,7,7,1,1,1,1,71,72,72,72,72,72,72,87,1,1,1,78,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,71,72,72,72,72,87,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,7,7,7,7,7,7,7,78,7,7,7,7,7,7,7,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,78,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,7,7,7,7,7,7,7,71,87,7,7,7,7,7,7,7,7,1,1,1,80,1,1,1,1,1,1,1,1,1,1,85,86,86,79,86,86,86,86,86,86,86,86,86,86,86,86,16,1,1,1,1,1,1,1,1,1,1,1,1,85,72,72,72,72,73,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,7,7,7,7,7,7,7,71,87,7,7,7,7,7,7,7,7,7,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,85,72,72,72,72,72,67,87,1,1,1,1,1,1,1,1,1,1,7,7,7,7,7,7,7,7,80,7,7,7,7,7,7,7,7,7,7,7,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,71,72,72,72,72,87,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,7,7,7,7,7,7,7,7,7,16,7,7,7,7,7,7,7,7,7,7,7,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,7,7,7,7,7,7,7,7,7,7,78,7,7,7,7,7,7,7,7,7,7,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,85,86,86,86,86,86,86,86,86,73,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,7,7,7,7,7,7,7,7,7,7,71,87,7,7,7,7,7,7,7,7,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,85,72,72,73,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,7,7,7,7,7,7,7,7,7,7,78,7,7,7,7,7,7,7,7,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,16,72,72,67,72,72,72,87,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,71,72,72,72,73,7,7,7,7,7,7,7,7,7,78,7,7,7,7,7,7,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,80,1,1,1,85,86,73,7,7,7,7,7,7,7,78,7,7,7,7,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,80,1,1,1,1,1,85,86,73,7,7,7,7,7,78,7,7,1,1,1,1,1,1,1,1,1,71,16,72,87,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,16,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,85,72,72,72,72,16,1,1,1,1,1,1,1,80,7,7,7,7,71,87,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,78,7,7,7,71,87,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,85,73,7,71,87,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,85,67,87,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,71,86,86,86,86,86,86,86,87,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,71,72,72,72,79,72,72,72,72,72,73,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,80,1,1,1,1,1,85,72,72,72,72,72,72,16,86,86,73,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,71,86,87,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,71,72,87,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,85,72,72,72,72,73,1,1,1,1,85,72,67,86,86,86,86,86,79,86,86,86,86,86,86,86,16,86,86,86,86,86,67,86,86,86,87,1,1,1,1,1,1,71,72,72,72,72,87,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,78,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,85,72,73,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,78,1,1,1,1,71,87,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,16,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,79,86,86,86,86,86,86,66,86,86,86,86,87,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,85,73,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,16,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,85,73,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,79,72,72,72,72,72,72,72,72,72,86,16,72,86,86,86,86,86,86,73,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,16,72,72,72,72,72,72,79,72,72,72,72,72,16,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,85,86,86,86,73,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,85,86,86,86,86,86,16,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,71,72,72,72,72,16,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,71,72,72,66,72,73,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,16,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,71,72,72,16,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,16,72,72,87,1,1,1,1,80,1,1,1,71,72,72,72,72,72,72,72,72,72,72,72,72,72,72,87,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,85,73,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,85,72,72,73,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,71,72,72,79,72,72,72,87,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,85,73,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,85,73,1,1,1,16,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,85,73,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,71,86,66,72,72,72,72,72,72,72,72,72,73,1,1,1,1,1,1,1,1,71,72,72,72,87,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,85,73,71,87,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,16,86,73,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,85,72,72,72,72,72,72,72,16,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,85,87,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,85,73,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,85,72,73,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,71,72,73,1,1,1,1,1,1,1,85,72,72,72,73,1,1,1,1,1,1,85,72,16,72,73,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,85,87,1,78,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,78,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,71,72,72,72,72,16,72,72,73,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,78,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,71,86,16,86,86,86,73,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,71,72,72,73,1,78,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,78,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,71,87,1,1,1,1,1,85,73,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,80,1,71,87,1,78,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,85,72,72,72,79,72,72,72,72,72,72,16,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,71,87,1,1,1,1,1,1,1,85,73,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,80,1,78,1,1,85,73,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,71,87,1,1,1,1,1,1,1,1,1,85,73,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,80,1,78,1,1,1,80,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,67,86,87,1,1,1,1,1,1,1,1,1,1,1,85,73,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,80,1,85,86,86,86,87,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,85,72,72,72,73,1,1,1,1,1,1,78,1,1,1,1,1,1,1,85,86,86,86,86,86,86,72,72,87,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,16,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,85,67,86,86,86,86,86,87,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,71,87,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,71,87,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,16,1,1,1,1,1,1,1,1,1,1,1,1,1,7,7,7,7,3,3,3,3,3,3,3,3,3,3,7,7,7,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,71,72,72,72,72,72,16,72,72,72,72,72,73,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,71,16,86,87,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,7,7,7,7,3,3,3,3,3,3,3,3,3,3,3,3,7,7,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,71,86,86,86,87,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,7,7,7,7,7,3,3,3,3,3,16,86,86,86,73,3,3,7,7,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,71,86,86,86,87,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,7,7,7,7,3,3,3,3,3,3,3,3,3,78,3,3,7,7,1,1,1,1,1,1,1,71,86,86,86,86,86,86,86,86,86,87,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,71,86,86,86,66,72,72,73,1,1,1,1,1,1,1,1,1,1,1,7,7,3,3,3,3,3,3,3,3,3,78,3,7,7,7,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,71,86,86,86,86,86,86,86,86,87,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,7,7,3,3,3,3,3,3,3,3,85,73,7,13,7,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,7,13,13,13,13,13,13,13,13,13,13,80,13,13,13,1,1,1,1,1,1,71,87,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,85,72,72,72,72,16,86,86,87,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,13,13,13,13,13,13,13,13,13,13,7,80,7,13,7,1,1,1,1,1,1,85,16,73,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,13,13,13,13,13,13,13,13,13,13,7,7,80,7,7,7,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,13,13,13,13,13,13,13,13,13,13,7,3,3,80,3,3,7,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,16,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,16,1,1,1,1,1,1,1,1,1,13,13,13,13,13,13,13,13,13,13,7,7,3,3,80,3,3,7,1,1,1,1,1,1,1,1,80,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,13,13,13,13,13,13,13,13,7,7,7,3,3,16,3,3,7,1,1,1,1,1,1,1,1,85,86,86,86,86,16,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,13,13,13,13,13,13,7,7,7,7,3,3,80,3,3,7,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,7,13,13,13,13,7,71,86,86,86,86,87,3,3,7,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,78,1,1,1,1,1,1,1,1,1,1,1,1,7,13,13,13,13,7,80,7,7,7,7,7,7,7,7,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,85,86,86,86,86,86,86,86,86,86,86,86,86,86,86,86,86,86,86,87,7,7,7,7,7,7,7,7});

      // Add settlements.
      f_settlement(registry, 10, 10, "Enemy City 1", texture, npc);
      f_settlement(registry, 20, 10, "Enemy City 2", texture, npc);
      
      // Add army.
      auto human_handle = game->texture_cache.handle("spritesheet/human"_hs);
      const auto &human_texture = human_handle;
      const auto &alien_texture = game->texture_cache.handle("spritesheet/alien"_hs);
      f_army(registry, 1, 1, human_texture, npc);
      f_army(registry, 2, 1, human_texture, npc);
      f_army(registry, 3, 1, human_texture, npc);
      f_army(registry, 4, 1, human_texture, npc);
      f_army(registry, 5, 1, human_texture, npc);
      f_army(registry, 6, 1, human_texture, npc);
      f_army(registry, 7, 1, human_texture, npc);
      f_army(registry, 8, 1, human_texture, npc);
      f_army(registry, 9, 1, alien_texture, player);
      f_army(registry, 10, 1, alien_texture, player);
      f_army(registry, 11, 1, alien_texture, player);
      f_army(registry, 12, 1, alien_texture, player);
      f_army(registry, 13, 1, alien_texture, player);
      f_army(registry, 14, 1, alien_texture, player);
      f_army(registry, 15, 1, alien_texture, player);
      f_army(registry, 16, 1, alien_texture, player);
      f_army(registry, 17, 1, alien_texture, player);
      f_army(registry, 18, 1, alien_texture, player);
      
      // Init gui elements.
      init_gui();
    }

    void init_gui() {
      // Init gui elements.
      auto end_turn = tgui::Button::create();
      end_turn->setText("End Turn");
      end_turn->setSize(80, 80);
      end_turn->connect("pressed", [=](){
        pass_turn();
      });
      gui.add(end_turn, "end_turn");
      refresh_gui();
    }

    // Position the gui elements and refresh position on resize.
    void refresh_gui() {
      auto end_turn = gui.get<tgui::Button>("end_turn");
      end_turn->setPosition(window.getSize().x - 80.f, window.getSize().y - 80.f);
    }

    void end_turn() {
      // End all turns.
      printf("Ending turn...\n");
      registry.view<movement_component>().each([](const auto entity, auto &mov) {
        mov.movement_points = mov.max_movement_points;
      });
    }

    void pass_turn() {
      // Pass the turn to the next player.
      player_turn++;
      if (player_turn >= game->players.size()) {
        player_turn = 0;
        end_turn();
      }
      printf("Passing turn to player %zu\n", player_turn);
      ai_play();
    }

    // If the player is ai, do whatever he needs to do.
    void ai_play() {
      auto entity = game->players[player_turn];
      auto &player = registry.get<player_component>(entity);

      switch (player.ai) {
        case E_ai::NONE:
          break;

        case E_ai::PLACEHOLDER:
        case E_ai::PASSIVE:
          pass_turn();
          break;
      }
    }

};
