#include "systems.hpp"
#include <cmath>
#include <stdio.h>
#include "fsm.hpp"
#define PI 3.14159265

void movement_system(entt::registry &registry, const int dt) {
  registry.view<movement_component, animate_component, position_component>().each([dt](const auto, auto &mov, auto &ani, auto &pos) {
    if (mov.destinations.size() > 0) {
      // Make sure its walking.
      transition_animation_state(E_state::WALK, ani);
      // Get first item on destination, move the entity and delete it.
      // ani.position > next.pos
      sf::Vector2f &sprite_position = ani.position;
      const float factor = 0.2; // Thats the game speed, @todo unhardcode.
      float movement = dt * factor; // How much the entity will move on this iteration.
      while (mov.destinations.size() > 0 && movement > 0) {
        auto next = mov.destinations.front();
        if (next.cost > mov.movement_points) {
          transition_animation_state(E_state::IDLE, ani);
          break;
        }
        sf::Vector2f next_tile_position{(float) next.pos.x * TILE_SIZE, (float) next.pos.y * TILE_SIZE};
        const float side_x = next_tile_position.x - sprite_position.x;
        const float side_y = next_tile_position.y - sprite_position.y;
        const float distance = sqrt(side_x * side_x + side_y * side_y); // Distance from the destination, (hypotenuse).
        // When arriving decide on the next direction the entity will be
        // facing.
        auto angle = (atan2(side_y, side_x) * 180 / PI) - 270; // 0 is north
        while (angle < 0) {
          angle = 360 + angle;
        }
        ani.direction = static_cast<E_direction>(((int) round(angle / 45)) % 8);
        // If its enough to get there, then lets move the entity there and
        // remove the destination from the array.
        if (movement >= distance) {
          sprite_position = next_tile_position;
          movement -= distance;
          mov.destinations.erase(mov.destinations.begin());
          pos = next.pos;
          mov.movement_points -= next.cost;
        }
        // Else, calculate the new position and set movement to 0.
        else {
          const float sin = fabs(side_x) / distance;
          const float step_x = movement * sin * (side_x > 0 ? 1 : -1);
          const float step_y = sqrt(movement * movement - step_x * step_x) * (side_y >= 0 ? 1 : -1);
          sprite_position += sf::Vector2f{step_x, step_y};
          movement = 0;
        }
      }
    }
    else {
      transition_animation_state(E_state::IDLE, ani);
    }
  });
}

// Return the selected entity on click.
std::vector<entt::entity> world_handle_click(entt::registry &registry, const sf::Vector2f mouse, entt::entity player) {
  std::vector<entt::entity> selected;

  // Select a unit if its on range of mouse.x and mouse.y.
  // @todo if its already selected move to the next unit.
  int tile_x = mouse.x / TILE_SIZE;
  int tile_y = mouse.y / TILE_SIZE;
  registry.view<position_component, select_component>().each([tile_x, tile_y, &selected, &player](const auto entity, auto &pos, auto &sel) {
    if (player == sel.player) {
      if (pos.x == tile_x && pos.y == tile_y) {
        sel.selected = true;
        selected.push_back(entity);
      }
      else {
        sel.selected = false;
      }
    }
  });
  return selected;
}

void render_pathfind_system(entt::registry &registry, std::vector<S_path> pathfinding_route, std::vector<entt::entity> selected_entity, sf::RenderWindow &window) {
  sf::CircleShape dot(4.f);
  dot.setOrigin(4.f, 4.f);

  auto &mov = registry.get<movement_component>(selected_entity.front());
  double cost_so_far = 0;
  for (S_path p : pathfinding_route) {
    cost_so_far += p.cost;
    if (cost_so_far > mov.movement_points + mov.max_movement_points) {
      dot.setFillColor(sf::Color(0, 0, 255));
    }
    else if (cost_so_far > mov.movement_points) {
      dot.setFillColor(sf::Color(255, 0, 0));
    }
    else {
      dot.setFillColor(sf::Color(255, 255, 102));
    }

    dot.setPosition(p.pos.x * TILE_SIZE + TILE_SIZE / 2, p.pos.y * TILE_SIZE + TILE_SIZE / 2);
    window.draw(dot);
  }
}

// Common rendering.
void render_system(entt::registry &registry, sf::RenderWindow &window) {
  // Print settlements.
  registry.group<render_component, settlement_component>().each([&window](const auto entity, auto &ren, auto &map) {
    window.draw(ren.sprite);
  });

  // Render animations.
  registry.view<animate_component>().each([&window](const auto, auto &ani) {
    auto sprite = ani.get_sprite();
    sprite.setPosition(ani.position);
    window.draw(sprite);
  });

  // @todo move to his own world render function.
  // Print name on top of settlements.
  sf::Text text;
  sf::Font font;
  font.loadFromFile("assets/arial.ttf");
  text.setFont(font);
  text.setCharacterSize(10);
  text.setFillColor(sf::Color(200, 200, 200));
  registry.view<metadata_component, settlement_component, select_component, render_component>().each([&window, &text](const auto entity, auto &met, auto &set, auto &sel, auto &ren) {
    text.setString(met.name);
    sf::FloatRect textRect = text.getLocalBounds();
    text.setOrigin((int) textRect.left - TILE_SIZE / 2 + textRect.width/2.0f, 0);
    if (sel.selected) {
      text.setFillColor(sf::Color(255, 200, 0));
    }
    else {
      text.setFillColor(sf::Color(200, 200, 200));
    }
    auto pos = ren.sprite.getPosition();
    text.setPosition(pos.x, pos.y - TILE_SIZE);
    window.draw(text);
  });

}

// dt = delta time, the time elapsed since last call to that function.
void animation_system(entt::registry &registry, const int dt) {
  // Animate.

  // Order elements. @todo optimize to a custom solution, apply culling so we
  // don't try to render something outside viewport, find a solution that
  // only re-sort the items we want to re-sort, in this case, remove the moving
  // items and add them again to sort it.
  registry.sort<animate_component>([](const auto &lhs, const auto &rhs) {
      return lhs.position.y < rhs.position.y;
  });

  registry.view<animate_component>().each([&dt](const auto, auto &ani) {
    if (ani.animations.find(ani.state) != ani.animations.end()) {
      int total = ani.animations[ani.state][ani.direction].size();
      // Only animate if there is more than 1 sf::Sprite.
      if (total > 1) {
        // Sum the accumulated time with delta, if the accumulated is bigger
        // than the sprite duration, we need to do a transition to the next
        // slide.
        ani.accumulated_time += dt;
        auto sprite = ani.get_animation_sprite();
        if (ani.accumulated_time >= sprite.duration) {
          ani.accumulated_time -= sprite.duration;
          ani.state_sprite_delta++; // Next sprite.
          if (ani.state_sprite_delta == total) { // Loop if thats the last slide.
            ani.state_sprite_delta = 0;
          }
        }
      }
      else {
        ani.state_sprite_delta = 0;
      }
    }
  });
}

// Armies are properly rendered at the render_system() function, this one just
  // render a list of armies using gui.
/*void render_armies_system(entt::registry &registry, game::game * game) {
  auto window = game->window;
  auto player = game->player;
  
  registry.view<army_component>().each([&player](const auto, auto &arm) {
    if (army_component.player == player) {
      // @todo use some element to print the armies here.
    }
  });

}*/
