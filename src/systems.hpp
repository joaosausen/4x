#pragma once
#define TILE_SIZE 16
#include <entt/entity/registry.hpp>
#include <SFML/Graphics.hpp>
#include <math.h> 
#include "components.hpp"

// Common systems that can be used by multiple scenes.

// Helper functions.
std::vector<entt::entity> world_handle_click(entt::registry&, const sf::Vector2f, entt::entity);

// Systems.
void movement_system(entt::registry&, const int);
void animation_system(entt::registry&, const int);
void render_system(entt::registry&, sf::RenderWindow&);
void render_pathfind_system(entt::registry&, std::vector<S_path>, std::vector<entt::entity>, sf::RenderWindow&);
//void render_armies_system(entt::registry&, game::game *);
