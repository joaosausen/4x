//https://gist.github.com/timdown/021d9c8f2aabc7092df564996f5afbbf
(function($) {

  /* Constants.
  –––––––––––––––––––––––––––––––––––––––––––––––––– */
  const TILE_SIZE = 16;


  /* Classes.
  –––––––––––––––––––––––––––––––––––––––––––––––––– */

  /**
   * Sprite class.
   *
   * Contains the bounds that are a array with 4 elements ([x, y, w, h]) and the
   * origin that is a array with 2 elements ([x, y]) that is related to the
   * bounds x,y.
   */
  class Sprite {
    constructor(bounds, origin = null) {
      this.bounds = bounds;
      if (origin === null) {
        // Middle.
        this.origin = [
          Math.floor(bounds[2] / 2),
          Math.floor(bounds[3] / 2)
        ];
      }
      else {
        this.origin = origin;
      }
    }
  }

  /**
   * Animation class.
   *
   * sprites_time is a array of arrays ([sprite_id, time]), time is in ms.
   * name is a machine name, any string is ok.
   */
  class Animation {
    constructor() {
      // [id, time], the sprite id and the animation time in milliseconds.
      this.sprites_time = [];
      this.name = '';
    }

    add_sprite_time(id, time) {
      this.sprites_time.push([id, time]);
    }
  }

  /* Initial variables.
  –––––––––––––––––––––––––––––––––––––––––––––––––– */

  // Canvas.
  let canvas = document.getElementById('canvas');
  let ctx = canvas.getContext('2d');
  let canvas_demo = document.getElementById('canvas_demo');
  let ctx_demo = canvas_demo.getContext('2d');

  // Origin pos for demo canvas.
  let demo_origin = [Math.floor(canvas_demo.width / 2), Math.floor(canvas_demo.height / 2 + 50)];

  // State.
  let state = 'none';

  let demo_playing = false;

  // Sprites.
  let new_sprite_start = {};
  let new_sprite_end = {};
  let new_sprite = {};
  let sprites = [];

  // Animations.
  let animations = [];

  // Demo animation.
  let current_sprite_id = 0;

  // UI.
  let sprites_list = $("#sprites_list");
  let animations_list = $("#animations_list");
  let animations_sprite_list = $("#animations_sprite_list");

  // The image loaded in the canvas.
  let img = new Image();

  /* Free functions.
  –––––––––––––––––––––––––––––––––––––––––––––––––– */
  let trimAll = function() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.drawImage(img, 0, 0);
    for (i in sprites) {
      sprites[i].bounds = trim(sprites[i].bounds, [0, 0, 0, 0]);
    }
    render();
  };

  // bgColor is a array with 4 items, rgba.
  let trim = function(sprite, bgColor) {
    // imageData is a array that goes on horizontal starting at top left and
    // ending on bottom right, every 4 items is the RGBA for a single pixel.
    let imageData = ctx.getImageData(...sprite);
    let width = imageData.width;
    let height = imageData.height;

    // i is the position of the r element.
    let isBlank = (i) => {
      if ((bgColor[0] != imageData.data[i])
      || (bgColor[1] != imageData.data[i+1])
      || (bgColor[2] != imageData.data[i+2])
      || (bgColor[3] != imageData.data[i+3])) {
        return false;
      }
      return true;
    };

    // Line start in 0.
    let blankLine = function(line) {
      // i starts at r on the line of number "line"
      for (let i = line * width * 4; i < (line + 1) * width * 4; i += 4) {
        if (!isBlank(i)) {
          return false;
        }
      }
      return true;
    };

    // Row start in 0.
    let blankRow = function(row) {
      // i starts at r on the line of number "line"
      for (let i = row * 4; i < width * height * 4; i += width * 4) {
        if (!isBlank(i)) {
          return false;
        }
      }
      return true;
    };
    let x = 0, y = 0, w = width, h = height - 1;

    while (blankRow(x) && x <= w) x++;
    while (blankRow(w) && w >= x) w--;
    while (blankLine(y) && y <= h) y++;
    while (blankLine(h) && h >= y) h--;
    new_sprite = [
      sprite[0] + x, // x
      sprite[1] + y, // y
      w - x,        // w
      h + 1 - y,    // h
    ];
    return new_sprite;
  };

  let draw_image = function() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.drawImage(img, 0, 0);
  };
  
  // Render the demo sprites.
  let render_demo = function(step = 1) {
    // Move iterator and loop if necessary.
    let last = animations_sprite_list.find('option').length - 1;
    let next_id = current_sprite_id + step;
    if (step > 0 && next_id > last) {
      current_sprite_id = 0;
    }
    else if (step < 0 && next_id < 0) {
      current_sprite_id = last;
    }
    else {
      current_sprite_id = next_id;
    }
    
    draw_image();
    let selected_animation = animations_list.find(':selected:first');
    let id = selected_animation.val();
    if (selected_animation.length) {
      let sprite_id = animations[id].sprites_time[current_sprite_id][0];
      if (sprite_id == undefined) {
        sprite_id = animations[id].sprites_time[0];
      }
      if (sprite_id == undefined) {
        return;
      }
      let image_data = ctx.getImageData(...sprites[sprite_id].bounds);
      ctx_demo.clearRect(0, 0, canvas_demo.width, canvas_demo.height);

      // Calculate the position based on sprite and canvas origin.
      let sprite_origin = sprites[sprite_id].origin;
      // demo_origin
      let posx, posy;
      posx = demo_origin[0] - sprite_origin[0];
      posy = demo_origin[1] - sprite_origin[1];
      ctx_demo.putImageData(image_data, posx, posy);

      
      let counter = `${current_sprite_id}/${last} | Sprite ${sprite_id}`;
      ctx_demo.fillText(counter, 5, 10);
    }

    // Render origin.
    //demo_origin

    ctx_demo.lineWidth = 1;
    ctx_demo.strokeStyle = "#0f0";
    ctx_demo.beginPath();
    ctx_demo.moveTo(demo_origin[0], 0);
    ctx_demo.lineTo(demo_origin[0], canvas_demo.height);
    ctx_demo.moveTo(0, demo_origin[1]);
    ctx_demo.lineTo(canvas_demo.width, demo_origin[1]);
    ctx_demo.stroke();
    render();

    if (demo_playing) {
      setTimeout(() => { render_demo(); }, animations[id].sprites_time[current_sprite_id][1]);
    }
  };

  let render = function() {
    ctx.lineWidth = 2;
    draw_image();

    let selected_sprites = sprites_list.val();
    for (i in sprites) {
      ctx.beginPath();
      if (sprites_list.find("option[value=" + i + "]").length == 0) {
        sprites_list.append(`<option value="${i}">Sprite ${i}</option>`);
      }
      if (selected_sprites.indexOf(i) >= 0) {
        ctx.strokeStyle = "#f00";
      }
      else {
        ctx.strokeStyle = "#000";
      }
      ctx.rect(...sprites[i].bounds);
      ctx.stroke();
    }

    for (i in animations) {
      let a = animations[i];
      if (animations_list.find("option[value=" + i + "]").length == 0) {
        animations_list.append(`<option value="${i}">Animation ${i}</option>`);
      }
    }

    let selected_animation = animations_list.find(':selected:first');
    if (selected_animation.length) {
      let id = selected_animation.val();
      animations_sprite_list.html('');
      for (i in animations[id].sprites_time) {
        let s = animations[id].sprites_time[i];
        let a = `${s[0]} (${s[1]}ms)`;
        animations_sprite_list.append(`<option value="${a}">Sprite ${a}</option>`);
      }
    }
  };

  let load_image_to_canvas = function() {
    $(canvas).attr('height', img.height).attr('width', img.width);
    ctx.drawImage(img, 0, 0);
  };

  let get_mouse_pos = function(e) {
    var rect = canvas.getBoundingClientRect();
    return {
      x: e.clientX - rect.left,
      y: e.clientY - rect.top
    };
  };

  let log = function(text) {
    // $("#log").append(text + "<br>");
  };

  let generate_code = function(mirrored = false) {
    console.log('generate_code');
    for (a in animations) {
      let format = `var = {` + '\n';
      for (s in animations[a].sprites_time) {
        let sprite_id = animations[a].sprites_time[s][0];
        animations[a].sprites_time
        let sprite = sprites[sprite_id];
        let time = animations[a].sprites_time[s][1];
        let bounds = sprite.bounds;
        if (mirrored) {
          bounds[0] += bounds[2];
          bounds[2] *= -1;
        }
        format += `  {{texture, {${bounds.join(', ')}}}, ${time}, {${sprite.origin.join(', ')}}},` + '\n';
      }
      format += `};` + '\n';
      console.log(format);
    }
  };

  $(document).ready(() => {
    $("[name=load_file]").change((e) => {
      if (e.target.files.length) {
        let file = e.target.files[0];
        if (file.type.match('image.*')) {
          let reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = (e) => {
            if (e.target.readyState == FileReader.DONE) {
              img.src = e.target.result;
              setTimeout(() => { load_image_to_canvas(); }, 100);
              log("Image loaded.");
            }
          }    
        } else {
          alert("Not a valid image.");
          $(e.target).val('');
        }
      }
    });

    sprites_list.add(animations_list).change(function(e) {
      render();
    });

    $(document).keypress((e) => {
      let keycode = e.which;
      console.log('keycode', keycode);

      switch (keycode) {
        case 115: // S, new sprite.
          state = 'new_sprite';
          break;

        case 100: // D, delete last sprite.
          sprites.pop();
          sprites_list.find('option:last').remove();
          render();

        case 103: // G, generate code
          generate_code();
          break;

        case 109: // M, generate code mirrored
          generate_code(true);
          break;
      }
    });

    $(document).mousedown((e) => {
      if (state == 'new_sprite') {
        new_sprite_start = get_mouse_pos(e);
      }
    });

    $(document).mouseup((e) => {
      if (state == 'new_sprite') {
        new_sprite_end = get_mouse_pos(e);
        new_sprite = new Sprite([
          Math.min(new_sprite_start.x, new_sprite_end.x),
          Math.min(new_sprite_start.y, new_sprite_end.y),
          Math.abs(new_sprite_start.x - new_sprite_end.x),
          Math.abs(new_sprite_start.y - new_sprite_end.y)
        ], null);
        sprites.push(new_sprite);
        state = 'none';
        render();
      }
    });

    $("#trim").click(() => {
      trimAll();
      render();
    });
    $("#delete").click(() => {
      sprites = [];
      sprites_list.html('');
      render();
    });
    $("#new_animation").click(() => {
      animations.push(new Animation());
      render();
    });
    $("#delete_animation").click(() => {
      let selected_animation = animations_list.find(':selected:first').index();
      animations.splice(selected_animation, 1);
      animations_list.html('');
      render();
    });
    $("#add_sprite_to_animation").click(() => {
      let selected_sprite = sprites_list.find(':selected');
      let selected_animation = animations_list.find(':selected');
      if (selected_sprite.length && selected_animation.length) {
        selected_sprite.each(function() {
          animations[selected_animation.val()].add_sprite_time(parseInt($(this).val()), 250);
        });
      }
      render_demo();
    });
    $("#remove_sprite_from_animation").click(() => {
      let selected_animation = animations_list.find(':selected:first').index();
      let selected_sprite_animation = animations_sprite_list.find(':selected:first').index();
      
      animations[selected_animation].sprites_time.splice(selected_sprite_animation, 1);
      animations_sprite_list.html('');      
      render();
    });

    let move_animation_sprite = function(step) {
      let selected_animation_sprite = animations_sprite_list.find(':selected:first');
      let selected_animation = animations_list.find(':selected:first');
      if (selected_animation_sprite.length && selected_animation.length) {
        let animation_id = selected_animation.val();
        let id = selected_animation_sprite.index();
        if (animations[animation_id].sprites_time[id+step] != undefined) {
          [animations[animation_id].sprites_time[id], animations[animation_id].sprites_time[id+step]] = [animations[animation_id].sprites_time[id+step], animations[animation_id].sprites_time[id]];
          render();
          animations_sprite_list.find(':eq(' + (id+step) + ')').attr('selected', 'selected');
        }
      }
    };
    
    $("#move_animation_sprite_down").click(() => {
      move_animation_sprite(1);
    });
    $("#move_animation_sprite_up").click(() => {
      move_animation_sprite(-1);
    });

    $("#canvas_demo_prev").click(() => {
      render_demo(-1);
    });
    $("#canvas_demo_next").click(() => {
      render_demo(1);
    });
    $("#canvas_demo_play").click(() => {
      demo_playing = true;
      render_demo(1);
    });
    $("#canvas_demo_stop").click(() => {
      demo_playing = false;
    });

    let move_origin = function(step) {
      let origin_all = $("#origin_all").is(':checked');
      let selected_animation = animations_list.find(':selected:first').val();
      if (origin_all) {
        let selected_sprites = sprites_list.val();
        for (i in selected_sprites) {
          let sprite_id = selected_sprites[i];
          sprites[sprite_id].origin[0] += step[0];
          sprites[sprite_id].origin[1] += step[1];
        }
      }
      else {
        let sprite_id = animations[selected_animation].sprites_time[current_sprite_id][0];
        sprites[sprite_id].origin[0] += step[0];
        sprites[sprite_id].origin[1] += step[1];
      }
      render_demo(0);
    };
    
    $("#origin_left").click(() => {
      move_origin([1, 0]);
    });
    $("#origin_right").click(() => {
      move_origin([-1, 0]);
    });
    $("#origin_up").click(() => {
      move_origin([0, 1]);
    });
    $("#origin_down").click(() => {
      move_origin([0, -1]);
    });
  });


  render_demo();
  
}(jQuery));
