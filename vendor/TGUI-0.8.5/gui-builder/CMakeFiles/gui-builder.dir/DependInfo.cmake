# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/joao/workspace/4x_ecs/tests/TGUI-0.8.5/gui-builder/src/Form.cpp" "/home/joao/workspace/4x_ecs/tests/TGUI-0.8.5/gui-builder/CMakeFiles/gui-builder.dir/src/Form.cpp.o"
  "/home/joao/workspace/4x_ecs/tests/TGUI-0.8.5/gui-builder/src/GuiBuilder.cpp" "/home/joao/workspace/4x_ecs/tests/TGUI-0.8.5/gui-builder/CMakeFiles/gui-builder.dir/src/GuiBuilder.cpp.o"
  "/home/joao/workspace/4x_ecs/tests/TGUI-0.8.5/gui-builder/src/main.cpp" "/home/joao/workspace/4x_ecs/tests/TGUI-0.8.5/gui-builder/CMakeFiles/gui-builder.dir/src/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "TGUI_USE_CPP17"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "gui-builder/include"
  "include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/joao/workspace/4x_ecs/tests/TGUI-0.8.5/src/TGUI/CMakeFiles/tgui.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
